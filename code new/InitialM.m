function [M] = InitialM (Rating,k)
    [Nuser,Nmovie]= size(Rating)
    M=zeros(k,Nmovie);
    for j=1:Nmovie
        for i=1:k
            if i==1

                M(i,j)= sum ( Rating(: ,j) )/(Nuser-sum(Rating(:,j)==0));
            else
                M(i,j)=rand;
            end
        end
    end
end   
    