function [sumofregular] = sumoft(U,M,rating,lambda)
[k,n]=size(U);
[k,m]=size(M);
sumofregular=0;
for i= 1:n
    [a,b]=sumsqr(U(i));
    sumofregular=sumofregular+a*nnz(rating(i,:));
end

for i= 1:m
    [a,b]=sumsqr(M(i));
    sumofregular=sumofregular+a*nnz(rating(:,i));
end
sumofreglar=sumofregular*lambda;
end


