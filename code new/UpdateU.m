function [U]= UpdateU(M,rating,social,alpha,lambda,k)
    [Nuser,Nmovie]=size(rating);
    U=zeros(k,Nuser);
    parfor i=1:Nuser
        [Ai,Vi]=computeAi(M,i,rating,social,alpha,lambda);
        if(~isempty(Vi))
            ui=inv(Ai)*Vi;
        else
            kan=1;
            ui=zeros(k,1);
        end
        U(:,i)=ui';
    end
end
    