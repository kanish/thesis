function [RMSError]= calRMSE(PR,testdata,nf)
SumError=0;
[Nuser,Nmovie]=size(PR);
count=0;
for i=1:Nuser
    for j=1:Nmovie
        if(testdata(i,j)~=0)
            if(nf>5 && nf<10)
                if(PR(i,j)>5)
                SumError=SumError+(5-testdata(i,j))^2;
                end
                if (PR(i,j)<1)
                        SumError=SumError+(1-testdata(i,j))^2;
                end    
                if (PR(i,j)>=1 && (PR(i,j)<=5))   
                    SumError=SumError+(PR(i,j)-testdata(i,j))^2;
                end
                count=count+1;
        else
            SumError=SumError+(PR(i,j)-testdata(i,j))^2;
            count=count+1;
            end
        end
    end
end
RMSError=sqrt(SumError/count)
end
