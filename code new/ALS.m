function [U,M,Niter] = ALS(social,rating,alpha,lambda,k)
M=InitialM(rating,k);
Error1=20;
Error2=10;
Niter=0;
while(abs(Error1-Error2) > 0.001*Error2 | Niter<5)
       U =UpdateU(M,rating,social,alpha,lambda,k);
       M =UpdateM(U,rating,social,alpha,lambda,k);
       Error2=Error1
       PR=U'*M;
       sumofratingterm = sumofr(rating,PR,1);
       sumofregular = sumoft(U,M,rating,lambda);
       sumofsocial = sumofr(social,PR,alpha);
       Error1= sumofratingterm + sumofregular + sumofsocial
       Niter=Niter+1
end

end

       
       
