function [finalU,finalM,Niter] = ALS2(social,rating,alpha,lambda,k,probedata)
M=InitialM(rating,k);
Error1=20;
Error2=10;
Niter=0;
minerr=10;
[Nuser,Nmovie]=size(rating);
while(abs(Error1-Error2) > 0.0001 | Niter<10)
       U =UpdateU(M,rating,social,alpha,lambda);
       M =UpdateM(U,rating,social,alpha,lambda);
       PR=U'*M;
       count=0;
       if(Niter==0)
           finalU=U;
           finalM=M;
       end 
       count1=0;
       SumError=0;
       SumError1=0;
       for i=1:Nuser
         for j=1:Nmovie
             if(probedata(i,j)~=0)
                SumError=SumError+(PR(i,j)-probedata(i,j))^2;
                count=count+1;
             end

        end
       end
       Niter=Niter+1;
    Error1=Error2;
    Error2=sqrt(SumError/count)
    if(Error2<minerr)
        finalU=U;
        finalM=M;
    end
    
end
end


       
       
