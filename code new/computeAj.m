function [A,V] = computeAj(U,j,rating,social,alpha,lambda)
    [k,Nuser]=size(U);
    count=0;
    r=rating(:,j);
    s=social(:,j);
    Ij=[];
    Isj=[];
    coun=0;
    parfor i=1:Nuser
        if r(i)~=0
            Ij=[Ij i];
            count=count+1;
        end
    end
    Kj=s(Ij);
    Uj=U(:,Ij);
    Rj=r(Ij);
    
    E=eye(k);
    A=(1+alpha)*Uj*Uj'+ lambda*count*E;
    V= Uj*(Rj+alpha*Kj);
end

        