function [sumofratingterm] = sumofr(rating,PR,alpha)
[n,m]=size(rating);
sumofratingterm=0;
for i=1:n
    for j= 1:m
        if(rating(i,j)~=0)
            sumofratingterm=sumofratingterm+alpha*(rating(i,j)-PR(i,j))^2;
        end
    end
end
end
