
threshold=0.35;
[n,n1]=size(a);
socialmatrix=zeros(n,n1);
for i=1:n
    for j=i:n
        if(a(i,j)>threshold)
            socialmatrix(i,j)=1;
        else
            socialmatrix(i,j)=0;
        end
        if(i==j)
            socialmatrix(i,j)=0;
        end
    end
    
end