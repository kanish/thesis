N=25;
RMSE=zeros(N,N);
MAE=zeros(N,N);
Niteration=zeros(N);
lambda=0.065;
%L=zeros(5);
%M=zeros(5);
%J=zeros(5);
for i=1:3
    Testnum=i
    if(i==1)
         lambda=0.2;
    end
    if(i==2)
        lambda=0.1;
    end
    if(i==3)
        lambda=0.25;
    end
    if(i==4)
        lambda=0.5;
    end
    if(i==5)
        lambda=1;
    end
    if(i==6)
        lambda=2.5;
    end
    if(i==7)
        lambda=5;
    end
    for nf=3:5
        [a,b,c]=ALS2(social,rating,0,lambda,nf*5,probedata);
     
        temperror=calRMSE(a'*b,testdata)
        RMSE(i,nf)=temperror;
        %MAE(i,nf)=a;
        Niteration(i,nf)=c;
    end
end

    