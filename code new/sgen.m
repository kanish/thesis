function [rating srating]= sgen(sdata,traindata)
    [n1,n2]=size(traindata);
    srating=sparse(n1,n2);
    [r,c,v]=find(traindata);
    length=size(r);
    for i=1:length
        ratsum=sum(traindata(:,c(i)));
        count=nnz(traindata(:,c(i)));
        if (count~=0)
           srating(r(i),c(i))=ratsum/count;
        else
           srating(r(i),c(i))=traindata(r(i),c(i));
        end
    end
    rating=traindata;
end
