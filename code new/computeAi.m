function [A,V] = computeAi(M,i,rating,social,alpha,lambda)
    [k,Nmovie]=size(M);
    count=0;
    r=rating(i,:);
    s=social(i,:);
    Ii=[];
 
    count=0;
    parfor i=1:Nmovie
        if r(i)~=0
            Ii=[Ii i];
            count=count+1;
        end
        
    end
    Ki=s(:,Ii);
    Ri=r(:,Ii);
    Mi=M(:,Ii);
    
    E=eye(k);
    A=(1+alpha)*Mi*Mi'+ lambda*count*E;
    V= Mi*(Ri+alpha*Ki)';
end

        