function [M]= UpdateM(U,rating,social,alpha,lambda,k)
    [Nuser,Nmovie]=size(rating);
    M=zeros(k,Nmovie);
    parfor i=1:Nmovie
        [Aj,Vj]=computeAj(U,i,rating,social,alpha,lambda);
        if(~isempty(Vj))
            mj=inv(Aj)*Vj;
        else
            mj=zeros(k,1);
        end
        M(:,i)=mj;
    end
end
    