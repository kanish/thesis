function [traindata,testdata,probedata] = testcases(udata,percentage)
    [Nuser,Nmovie]=size(udata);
    count=nnz(udata);
    NumberofRatings=count
    traindata=udata;
    count1=0;
    testdata=sparse(Nuser,Nmovie);
    probedata=sparse(1,1);
    while (count1*100/count< percentage)
        I = randi([1 Nuser],1,1);
        J = randi([1 Nmovie],1,1);
        if (udata(I(1),J(1))~=0);
            traindata(I(1),J(1))=0;
            count1=count1+1;
            if rem(count1,2)==0
                   testdata(I(1),J(1))=udata(I(1),J(1));
            else
                testdata(I(1),J(1))=udata(I(1),J(1));
            end
        end
    end
    %traindata( all(~traindata,2), : ) = [];
    %traindata( :, all(~traindata,1) ) = [];
    NumberofTestdata=count1
        
end
   