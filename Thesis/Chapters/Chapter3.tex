% Chapter Template
\addtocontents{toc}{\protect}
\chapter{Low Rank Matrix Factorization} % Main chapter title

\label{Chapter3} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

\lhead{Chapter 3. \emph{Low Rank Matrix Factorization}} % Change X to a consecutive number; this is for the header on each page - perhaps a shortened title
In this chapter, we will discuss one of the popular classes of collaborative filtering techniques, matrix factorization in detail. Some of the most successful latent factor models are based on matrix factorization. In the first section, we formulate the problem and the approach, and then we discuss the solution to the problem. 
\section{Problem Formulation}
Matrix factorization techniques represent the users and the items in a joint latent factor space of $k$ dimensions and the inner product in that space models the user-item interaction. Each user and item are described using a $k$-dimensional feature vector. Let the user and item feature matrix be $U$ and $M$ respectively. The user $i$ and item $j$ are associated with $k$-dimensional vectors, $u_{i}$ and $m_{j}$ respectively. The predicted rating is computed using the relation given by Eq.~(\ref{hat}).
\begin{equation}
\label{hat}
\hat{r}_{ij}=u_{i}^{T} m_{j}
\end{equation}
To learn the feature vectors, we define a loss function $f(U,M)$ which is the regularized sum of the squared error:
\begin{equation}
\label{lossfun}
f(U,M)=\sum_{(i,j)\in I} ( r_{ij} - u_{i}^{T} m_{j})^2 + \lambda \left( \sum_{i} n_{u_{i}} \|u_{i}\|^2+ \sum_{j} n_{m_{j}}\|m_{j}\|^2 \right)
\end{equation}
where $r_{ij}$ is the rating given to item $j$ by user $i$, $n_{u_{i}}$ and $n_{m_{j}}$ are the number of movies rated by user $i$ and the number of users that rated the movie $j$ respectively.
The first term is simply the sum of the squared error and the second term is for regularization. It avoids overfitting as it penalizes the complexity of $U$ and $M$.
\begin{figure}
	\centering
		\includegraphics[width=6in,keepaspectratio]{Figures/approach.jpg}
	\caption[Matrix Factorization Approach]{Matrix Factorization Approach}
	\label{fig:approach}
\end{figure}
\begin{equation}
R\approx U^{T}M
\end{equation}
Now, an efficient algorithm is required to solve for $U$ and $M$  such that $U^{T} M$ approximates R, the ratings matrix.
\section{Learning Algorithms}
There are two algorithms that minimize the loss function and compute $U$ and $M$, namely, alternating least squares and stochastic gradient descent. Although stochastic gradient descent is faster, alternating least squares has its own advantages. The learning algorithm that we have adopted is alternating least squares. We first give a brief description of the two algorithms in this section and give the details of the update rules alternating least squares in subsequent sections. 
\subsection{Alternating Least Squares}
We consider the loss function $f(U,M)$. It is a convex function in $U$ for fixed $M$ and vice versa. Since it is a convex function, we can find a unique $U$ for a fixed $M$ for which the loss function is minimized. Similarly, we compute $M$ for the obtained $U$ and repeat this process until the stopping criterion is satisfied.
\begin{algorithm}[t]
\textbf{Input:} Rating matrix R\\
\textbf{Output:} User feature matrix U and item feature matrix M
\begin{algorithmic}[1]
\caption{Alternating Least Squares}\label{ALS}
\State Initialize $M$
\State $stop \gets false$
 \While{$stop$ = $false$ }
 \For {$i=1 \to nuser$}
 \State $ u_{i} \gets updateU(R,M,i) $
\EndFor
\For {$ j=1 \to nitem$}
   \State $ m_{j} \gets updateM(R,U,j) $
 \EndFor
 \If{\textit{ stopping criterion satisfied}}
\State $stop \gets true$
\EndIf
\EndWhile
\end{algorithmic}
\end{algorithm}
In the above mentioned algorithm, the updateU and updateM are functions which return the feature values based on the update rules. We shall discuss the update rules in detail, in section \ref{updaterules}.
\subsection{Stochastic Gradient Descent}
The algorithm iterates through the training set and updates the features in a direction opposite to the gradient. Essentially, it makes a random guess and computes the gradient of the loss function and makes a step in the direction of the gradient in order to minimize the loss function.
For each training case, the error is computed as shown in Eq.~(\ref{err}).
\begin{equation}
\label{err}
f_{ij}=r_{ij} - u_{i}^{T} m_{j}
\end{equation}
Then the feature vectors are modified proportionally in the opposite direction of the gradient as shown in Eq.~(\ref{sgdu}) and Eq.~(\ref{sgdm}).
\begin{equation}\label{sgdu}
u_{i}=u_{i} + \gamma . (f_{ij}.m_{j} - \lambda u_{i}) 
\end{equation}
\begin{equation}\label{sgdm}
m_{j}=m_{j} + \gamma . (f_{ij}.u_{i} - \lambda m_{j}) 
\end{equation}
where $\gamma$ is the learning rate and $\lambda$ is the constant for regularization.
\subsection {Alternating Least Squares versus Stochastic Gradient Descent}  
Both of the algorithms solve similar quadratic equations. It has been observed that a stochastic gradient descent iteration is faster than an ALS iteration as it involves a least squares solution which is heavier. However, fewer ALS iterations are typically needed for solving to the same level of accuracy and the extent of parallelization in ALS is more as all movies or users can be computed independently in parallel, while stochastic gradient descent may have slower parallel performance. The amount of data is very large, so efficient parallelization of the algorithm is essential. Hence, we adopt ALS as the preferred algorithm for matrix factorization.
\section{Update Rules for ALS}
\label{updaterules}
In this section, we give a detailed mathematical derivation of the update rules for $U$ and $M$. We compute the $U$ for which the loss function is minimum for a given $M$, similarly $M$ can be computed as well.
\begin{equation}
R\approx U^{T}M
\end{equation}
We consider the following loss function, minimize it with respect to $U$ for a given M:
\begin{equation}
f(U,M)=\sum_{(i,j)\in I} ( r_{ij} - u_{i}^{T} m_{j})^2 + \lambda \left(\sum_{i} n_{u_{i}} \|u_{i}\|^2+ \sum_{j} n_{m_{j}}\|m_{j}\|^2\right) 
\end{equation}
To find the minimum, we differentiate the loss function w.r.t. $U$. We denote the $k^{th}$ feature of user $i$ by $u_{ki}$.
\begin{equation}
 \frac{1}{2} \frac{df}{du_{ki}}=0 \; \; \forall i,k
\end{equation}
\begin{equation}
  \implies \sum_{j\in I_{i}} (u_{i}^{T} m_{j}-r_{ij}) m_{kj} + \lambda  n_{u_{i}} u_{ki}=0 \; \; \forall i,k
\end{equation}
\begin{equation}
 \implies \sum_{j\in I_{i}}m_{kj}{m_{j}^{T}u_{i}}+\lambda  n_{u_{i}} u_{ki}=\sum_{j\in I_{i}}m_{kj}(r_{ij}) \; \; \forall i,k
\end{equation}
\begin{equation}
\implies (M_{I_{i}} M_{I_{i}}^{T} +\lambda  n_{u_{i}}E )u_{i}=M_{I_{i}}(R(i,I_{i})^{T})\; \; \forall i
\end{equation}
\begin{equation}
 \implies u_{i}=A_{i}^{-1} V_{i} \; \; \forall i
\end{equation} 
where $A_{i}=( M_{I_{i}} M_{I_{i}}^{T} +\lambda  n_{u_{i}}E )$, $ V_{i}=M_{I_{i}}(R(I_{i},i))\;$ and $E$ is a $k \times k$ identity matrix. $M_{I_{i}}$ denotes the sub-matrix of $M$ in which all the columns are taken which user $i$ has rated and $R(i,I_{i})$ is a row vector which has columns that user $i$ has rated.\par
Similarly, we find the update rule for $M$, which is  $m_{j}=A_{j}^{-1} V_{j}, \; \forall j$, where $ V_{j}=U_{I_{j}}(R(I_{j},j))\;$ and $A_{j}=( U_{I_{j}} U_{I_{j}}^{T} +\lambda  n_{m_{j}}E )$.
%
\section{Scalability}
\label{scale}
In this section, we discuss the scalability of ALS. The algorithm is scalable as it can be parallelized. Parallelizing the computation of the update rules for $U$ and $M$, and storing the rating and social matrix in a distributed system makes ALS suitable for large size inputs. Scalability as mentioned in Chapter \ref{Chapter2} is one of the biggest challenges in collaborative filtering which parallel ALS tackles very well. To get an insight of parallelizing the computation of $U$ and $M$, we look at the update rules:
\begin{equation}
\label{updateu}
u_{i}=A_{i}^{-1} V_{i},  \;\forall i 
\end{equation}
\begin{equation}
\label{updatem}
m_{j}=A_{j}^{-1} V_{j}, \; \forall j\\
\end{equation}
We notice that for a given $M$, rows of $U$ are independent of the other rows of $U$ and similarly for $M$, the columns are independent. Therefore, computation for $U$ and $M$ can be parallelized.
\section{Different Loss Functions}
In this section, we discuss the modifications in the loss functions that can be used to improve the accuracy of the recommendation. The term that we are interested in minimizing is the first term of the loss function in Eq.~(\ref{lossfun}) but to avoid over fitting another term is added in the loss function.\par
Some of the recommender systems based upon matrix factorization \citep{yuan2011factorization,noel2012new,ma2011recommender} use different loss functions to incorporate additional constraints into the algorithm. In Chapter \ref{Chapter4}, we discuss our approach which is based on a modification of the loss function given by Eq.~(\ref{lossfun}) and solve it using alternating least squares.
