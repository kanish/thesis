% Chapter 1

\chapter{Introduction} % Main chapter title

\label{Chapter1} % For referencing the chapter elsewhere, use \ref{Chapter1} 

\lhead{Chapter 1. \emph{Introduction}} % This is for the header on each page - perhaps a shortened title

%----------------------------------------------------------------------------------------
{\it Software model checking} is the analysis of programs through various algorithms and prove properties
of their executions. As one can imagine, model checking is a power tool for analysis of programs. However,
model checking is computationally hard. As the complexity of software systems grows, the need for smarter
model checkers is increasing.

There are two ways to reduce the computations involved. One way is to come up 
with better and smarter algorithms. The other is to abstract models which emulate the behavior of the original
program to a large extent, but are simpler than the actual program.

As the softwares are becoming bigger and more complex, there is a need to automate the process of extracting these 
models. Automatically extracting models is an active area of research.

\zing~\citep{zingtools} is one such model checker. The \zing~model checking language is quite similar to C and many of its constructs translate to \zing~directly.

In this thesis, we discuss our tool, \zmex~which can extract models for the \zing~model checker \citep{zingtools}
from a program written in \cilkp~language. \cilkp~(or {\bf cilk plus}) is basically the \cilk~programming language \citep{cilk} with a few medications.

\zmex~uses \pyc~\citep{PyCParser} for parsing \cilkp~programs and generates a model written in the \zing~model checking language \citep{zinglang}.
The model generated is instrumented such that it is possible to trace the counter example to a path in the original program. This can facilitate a a feedback mechanism to 
automatically detect and fix errors.
We also present algorithms to translate \cilkp~constructs to their \zing~counterparts and design a data structure to store, access and modify thread information efficiently.
\bigskip
\begin{figure}[htbp]
	\centering
		\includegraphics[width=4in,keepaspectratio]{Figures/cegar.png}
	\caption[Automated Bug Fixing with \zmex]{Automated Bug Fixing with \zmex}
	\label{fig:cegar}
\end{figure}
The thesis is divided into 6 chapters. The first is introduction. Chapter \ref{Chapter2} talks about already existing research in the field. Chapter \ref{Chapter3} gives an overview of \cilkp~and \zmex~and the tools used in its development. Chapters \ref{Chapter4}, \ref{Chapter5} and \ref{Chapter6} talk about the actual implementation and has some worked out examples. Chapter \ref{Chapter7} has results
and details about the experiments conducted and Chapter \ref{Chapter8} talks about possible future work.
%----------------------------------------------------------------------------------------
\section{Contributions}
This thesis attempts to solve a very practical problem. Following are the contributions of this thesis

- Develop a tool to abstract \zing~models from \cilk~programs

- Design algorithms to facilitate joining of different threads

- Create a feedback loop to help identify bugs in the original program

- Give detailed analysis of the performance of the tool on the benchmark programs