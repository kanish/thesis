% Chapter Template

\chapter{Our Approach} % Main chapter title
\label{Chapter4} % Change X to a consecutive number for refencing this chapter elsewhere, use \ref{ChapterX}
\lhead{Chapter 4. \emph{Our Approach}} % Change X to a consecutive number; this is for the header on each page - perhaps a shortened title
In this chapter, we discuss the approach that we have adopted to predict the ratings of the users given their past rating history and the social connections among the users. To incorporate social connections we modify the learning algorithm ALS discussed in Chapter \ref{Chapter3} and refer to our modified learning algorithm as Social ALS.
\section{Problem Formulation}
We are given a $n\times m$ ratings matrix $R$ and a $n \times n$ social network matrix $S$, where $n$ is the number of users and $m$ is the number of items. The ratings matrix $R$ stores the ratings by representing different users with different row numbers and different movies with different column number. Essentially, this ratings matrix is sparse as all the users do not rate every item. The goal is to predict the ratings which do not exist in $R$ and recommend the items for which the predicted rating is higher for a given user.
\section{Social Matrix Factorization}
One of the most successful latent factor model technique in collaborative filtering is matrix factorization \citep{koren2009matrix}. We modify this technique to incorporate the social network as well to test our hypothesis. We propose an addition of a social connection term in the loss function that would penalize those predicted values of ratings which are far from what the user's friends have rated to a given item. 
\begin{equation}
f(U,M)=\sum_{(i,j)\in I} ( r_{ij} - u_{i}^{T} m_{j})^2 + \lambda \left(\sum_{i} n_{u_{i}} \|u_{i}\|^2+ \sum_{j} n_{m_{j}}\|m_{j}\|^2\right) + SC
\end{equation}
where $SC$ is the social connection term.
\subsection{Social Connection Term}
We design a social connection term such that the value of the term is high for predicted ratings that are farther from the average rating given to an item in the friend list. Penalizing the deviation from the average rating  is expected to put an extra constraint while determining $U$ and $M$. We define the social connection term to be:
\begin{equation}
  SC =\alpha \sum_{(i,j)\in I} \left(u_{i}^{T} m_{j}- \frac{1}{\|f_{i}\|}\sum_{l\in f_{i}} r_{lj}\right)^2
\end{equation}
where $\alpha$ is the social constant, and $f_{i}$ is the friend list of user $i$. The social constant $\alpha$ determines the weight of the social connection term in the loss function. Higher values of $\alpha$ suggest higher social influence in the feature matrices. \par
ALS is being an iterative algorithm, updates the feature vectors $u_{i}$ and $m_{j}$ in every iteration. The key idea of introducting a social connection term is to penalize the deviation of predicted ratings from the mean of the ratings given to an item by the users' friend list. Consequently, the algorithm modifies the feature vectors to reduce the deviation in the next iteration. The term is squared in order to keep the loss function a convex function as the sum of convex functions are convex. 
\subsection {Justification for the Social Connection Term }
 Predicting the rating that a user would give to an item is a tough task. Even the best of algorithms predict the ratings on real datasets with squared error around $1$, where ratings are being given on a scale of 1-5.\par 
We introduce a social connection term to put a constraint on the latent feature projection of the users and items by incorporating the information offered by the social network. Penalizing those prediction in the loss functions which are away from the mean of ratings given by the user's friends would model the users closer to their friends. The underlying hypothesis behind the penalizing is that the probability of a user having the same opinion about an item as her neighbor in the social network would be more compared to a user chosen randomly.    
\section{Learning Algorithm}
We modify the alternating least squares algorithm (Algorithm \ref{ALS}) to solve for $U$ and $M$ but in order to compute the feature matrices, we have to come up with the new update rules for new loss function. We will refer to this algorithm as Social ALS to differentiate it from ALS. Another crucial factor for creating a good model is the number of iterations in which $U$ and $M$ converge to the minima.
\begin{algorithm}
\textbf{Input:} Rating matrix R, social network S\\
\textbf{Output:} User feature matrix U and item feature matrix M
\begin{algorithmic}[1]
\caption{Social Alternating Least Squares}\label{SocialALS}
\State Initialize $M$
\State $stop \gets false$
  \While{$stop$ = $false$ }
  \For {$i=1 \to nuser$}
  \State $ u_{i} \gets updateU(R,M,i,S) $
\EndFor
\For {$ j=1 \to nitem$}
   \State $ m_{j} \gets updateM(R,U,j,S) $
  \EndFor
 \If{\textit{ stopping criterion satisfied}}
\State $stop \gets true$
\EndIf
   \EndWhile
\end{algorithmic}
\end{algorithm}
\par
In Algorithm \ref{SocialALS}, the updateU and updateM take social network $S$, into account and reflect the effect of social connection term. The details of the update rules which consider the social connection term are discussed in the next section.\par
The number of iterations depends on the stopping criterion. We stop the iterations when the value of loss function does not change by 0.1\% of the value in the previous iteration.  
\section{Update Rules for Social ALS}
In this section, we give a detailed mathematical derivation of the update rules for $U$ and $M$. We derive the update rules of $U$ for which the loss function is minimum for a given $M$, and similarly derive the update rules for $M$. For simplicity, we collect all the symbols and their meanings in Table~\ref{tab:symbols}.  
\begin{table}
\begin{center}
  \begin{tabular}{ l | c}
    \hline
    Symbol & Meaning \\  \hline
     $U$ & User feature matrix\\ 
     $M$ & Item feature matrix \\ 
     $I_{i}$ & Set of items which user $i$ has rated \\ 
     $I_{j}$ & Set of users who have rated item $j$ \\ 
     $M_{I_{i}}$ & Sub-matrix of M with items which user $i$ has rated \\ 
     $U_{I_{j}}$ & Sub-matrix of U with users who have rated item $j$  \\ 
     $n_{u_{i}}$ & Number of items rated by user $i$ \\ 
     $n_{m_{j}}$ & Number of users who have rated item $j$ \\ 
     $f_{i}$ & Friend list of user $i$ \\ 
     $\alpha$ & Social constant\\ 
     $\lambda$& Regularization constant \\ \hline  
\end{tabular}
   \caption[List of symbols]{List of symbols}
   \label{tab:symbols}
\end{center}
\end{table}
\subsection{Update Rule for User Feature Matrix} 
We wish to approximate $R$ by the feature matrices as in Eq.~(\ref{approxi}).
\begin{equation}
\label{approxi}
R \approx U^T M
\end{equation}
We modify the loss function in Eq.~(\ref{lossfun}) by adding the social connection term to it.
\begin{multline}
\label{socialoss}
f(U,M)=\sum_{(i,j)\in I} ( r_{ij} - u_{i}^{T} m_{j})^2 + \lambda \left(\sum_{i} n_{u_{i}} \|u_{i}\|^2+ \sum_{j} n_{m_{j}}\|m_{j}\|^2\right) + \\ \alpha\sum_{(i,j)\in I} \left(u_{i}^{T} m_{j}- \frac{1}{\|f_{i}\|}\sum_{l\in f_{i}} r_{lj}\right)^2
\end{multline}
To find the update rules for $U$, we differentiate the loss function in Eq.~(\ref{socialoss}) w.r.t. $U$.
\begin{equation}
\frac{1}{2} \frac{df}{du_{ki}}=0 \; \; \forall i,k
\end{equation}
\begin{equation}
\implies \sum_{j\in I_{i}} (u_{i}^{T} m_{j}-r_{ij}) m_{kj} + \lambda  n_{u_{i}} u_{ki} + \alpha \sum_{j\in I_{i}} \left(u_{i}^{T} m_{j} - \frac{1}{\|f_{i}\|}\sum_{l\in f_{i}} r_{lj}\right) m_{kj}=0 \; \; \forall i,k
\end{equation}
For the sake of convenience, we represent $\frac{1}{\|f_{i}\|}\sum_{l\in f_{i}} r_{lj}$ as $s_{ij}$ and refer to this matrix as $S$.
\begin{equation}
\implies \sum_{j\in I_{i}} (1+\alpha) m_{kj}{m_{j}^{T}u_{i}}+\lambda  n_{u_{i}} u_{ki}=\sum_{j\in I_{i}}m_{kj}(r_{ij}+ \alpha s_{ij}) \; \;\forall i,k
\end{equation}
\begin{equation}
\implies ((1+\alpha) M_{I_{i}} M_{I_{i}}^{T} +\lambda  n_{u_{i}}E ) u_{i}=M_{I_{i}}(R(i,I_{i})^{T}+\alpha S(i,I_{i})^{T})\; \; \forall i
\end{equation}
\begin{equation}
\implies u_{i}=A_{i}^{-1} V_{i} \; \;\forall i
\end{equation}
where $A_{j}=((1+\alpha) M_{I_{i}} M_{I_{i}}^{T} +\lambda  n_{u_{i}}E )$ and $ V_{i}=M_{I_{i}}(R(i,I_{j})^{T}+\alpha S(i,I_{i})^{T})$.
\subsection{Update Rule for Item Feature Matrix}
We consider the loss function with the social connection term:
\begin{multline}
\label{socialossm}
f(U,M)=\sum_{(i,j)\in I} ( r_{ij} - u_{i}^{T} m_{j})^2 + \lambda \left(\sum_{i} n_{u_{i}} \|u_{i}\|^2+ \sum_{j} n_{m_{j}}\|m_{j}\|^2\right) + \\ \alpha\sum_{(i,j)\in I} \left(u_{i}^{T} m_{j}- \frac{1}{\|f_{i}\|}\sum_{l\in f_{i}} r_{lj}\right)^2
\end{multline}
Now to find the update rules for $M$, we differentiate the loss function in Eq. (\ref{socialossm}) w.r.t. $M$.
\begin{equation}
\frac{1}{2} \frac{df}{dm_{kj}}=0 \; \; \forall j,k
\end{equation}
\begin{equation}
\implies \sum_{i\in I_{j}} (u_{i}^{T} m_{j}-r_{ij}) u_{ki} + \lambda  n_{m_{j}} m_{kj} + \alpha \sum_{i\in I_{j}} \left(u_{i}^{T} m_{j} - \frac{1}{\|f_{i}\|}\sum_{l\in f_{i}} r_{lj}\right) u_{ki}=0 \; \; \forall j,k
\end{equation}
\begin{equation}
\implies \sum_{i\in I_{j}} (1+\alpha) u_{ki}{m_{j}^{T}u_{i}}+\lambda  n_{m_{j}} m_{kj}=\sum_{i\in I_{j}}u_{ki}(r_{ij}+ \alpha s_{ij}) \; \;\forall j,k
\end{equation}
\begin{equation}
\implies ((1+\alpha) U_{I_{j} }U_{I_{j}}^{T} +\lambda  n_{m_{j}}E ) m_{j}=U_{I_{j}}(R(I_{j},j)+ \alpha S(I_{j},j))\; \; \forall j
\end{equation}
\begin{equation}
\implies m_{j}=A_{j}^{-1} V_{j} \; \forall j
\end{equation}
where $A_{j}=((1+\alpha) U_{I_{j}} U_{I_{j}}^{T} +\lambda  n_{m_{j}}E )$ and $ V_{j}=U_{I_{j}}(R(I_{j},j)+ \alpha S(I_{j},j))$.
The above modification has to ensure that the algorithm still remains scalable and it is scalable as it satisfies the condition discussed in section \ref{scale}.
\section{Parameter Tuning}
\label{partuning}
A major obstacle for getting optimal results with matrix factorization based collaborative filtering is determining the values of the parameters involved. We perform a grid search for finding the optimal or close to optimal values of the parameters. As the algorithm is computationally expensive, we have to carefully prune some regions before we perform a grid search.\par  
We leverage the fact that the error is some what a continous convex function in $\lambda$ with other parameters fixed. Let us denote the optimal value of $\lambda$ for a fixed $k$ by $\lambda_{opt}$ and $\lambda_{opts}$  in a non-social and a social setting respectively. For $\alpha=0$, the error for $\lambda_{opts}$ would be more than the error for $\lambda_{opt}$ as $\lambda_{opt}$ is the optimal $\lambda$ for non-social setting. Let us denote the difference in error and difference in the $\lambda$ values by $\Delta e$ and $\Delta \lambda$ respectively.\par   
We also expect that the error is continuous convex function in $\alpha$ for the range of values that we are interested in. Let us denote the maximum value of $\frac{\Delta e}{\Delta \alpha}$ by $\gamma$. For getting an improvement in error in a social setting, Eq.~(\ref{aerr}) must hold.
\begin{equation}
\label{aerr}
\Delta e < \gamma . \Delta \alpha
\end{equation}
\begin{equation}
\label{aerr1}
\implies \frac{\Delta e}{\Delta \lambda}.\Delta \lambda < \gamma . \Delta \alpha
\end{equation}
We will be able to prune the regions for the grid search if the condition given by Eq.~(\ref{gg}) is satisfied.
\begin{equation}
\label{gg}
\frac{\Delta e}{\Delta \lambda} \gg \gamma
\end{equation}
\begin{equation}
\label{alpha}
\implies \Delta \lambda \ll \Delta \alpha
\end{equation}
\par Hence, the range of $\lambda$ can be narrowed down to the vicinity of the optimal $\lambda$ observed for the grid search if Eq.~(\ref{gg}) holds true. Moreover, we do not expect much change in the optimal value of $\lambda$ if we vary the number of features. With a few trials, we can fix $\lambda$ and $k$ for which the algorithm yields good results and then we study the effect of $\alpha$ on the quality of recommendations generated.   

















