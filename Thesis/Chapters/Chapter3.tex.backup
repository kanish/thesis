% Chapter Template
\addtocontents{toc}{\protect\newpage}
\chapter{Overview} % Main chapter title

\label{Chapter3} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

\lhead{Chapter 3. \emph{Overview}} % Change X to a consecutive number; this is for the header on each page - perhaps a shortened title

In this section we fist present an introduction to the tools used and then an overview of our tool, \zmex~and our input language, \cilkp.

\section{Tools Used}
Two tools were used in the creation of \zmex.
\begin{description}
 \item[Microsoft \zing] The \zing~ project is an effort to build a flexible and scalable model checking infrastructure for concurrent software \citep{zingtools}.
 \item[\pyc] \pyc~is a tool for parsing the C language, written in Python \citep{PyCParser}.
\end{description}

%-----------------------------------
%	SECTION 1
%-----------------------------------
\subsection{Zing}
The \zing~project aims to create an infrastructure for model checking concurrent software. This infrastructure is meant for testing and validating software at various levels:
high-level protocol descriptions, work-flow specifications, web services, device drivers, and protocols in the core of the operating system. \zing~is currently being
used for developing drivers for Windows and Windows Phone.
The project has four components \citep{zingtools}
\begin{itemize}
 \item A modeling language for expressing concurrent models
 \item A compiler for translating these models into an executable
 \item A state space explorer for exploring the state space of \zing~models
 \item Model generators that automatically extract models from programs written in common programming languages
\end{itemize}
In a bid to support automatic extraction of \zing~models from programs, \zing's modeling language \citep{zinglang} supports the following three facilities present in all modern
programming languages: \citep{zingconcur}
\begin{itemize}
 \item Procedure calls with a  call-stack
 \item Dynamic allocation
 \item Processes with dynamic creation (using both shared memory and message passing for communication)
\end{itemize}
Since \zing~models have complicated features such as processes, heap and stack, building a scalable model checker
for the \zing~modeling language is a huge challenge. Thus, the \zing~compiler converts a \zing~model to a \zing~object model 
(or ZOM). ZOM is a simple view of the model as a labeled transition systems \citep{zingconcur}.
This separation allows the \zing~model checker to only focus on efficient model checking algorithms and 
not on supporting the rich constructs in the modeling language.
\bigskip
\begin{figure}[htbp]
	\centering
		\includegraphics[width=\textwidth,keepaspectratio]{Figures/zingarch.png}
	\caption[The \zing~architecture]{The \zing~architecture}
	\label{fig:zingarch}
\end{figure}

\subsection{Example of a Zing model}
Program~\ref{lst:zingeg} is the model of the dining philosopher's problem as available in the \zing~Language Specification \citep{zinglang}.
\begin{lstlisting}[numbers=left, caption=\zing~Example Program, label={lst:zingeg}]
class Fork {
  Philosopher holder;
  void PickUp(Philosopher eater) {
    atomic {
      select {
	wait(holder == null) -> holder = eater;
      }
    }
  }
  void PutDown() {
    holder = null;
  }
};

class Philosopher {
  Fork leftFork;
  Fork rightFork;
  void Run() {
    while (true) {
      // pick up forks
      leftFork.PickUp(this);
      rightFork.PickUp(this);
      // eat for a while
      leftFork.PutDown();
      rightFork.PutDown();
      // think for a while
    }
  }
};

array Philosophers[5] Philosopher;
array Forks[5] Fork;

class Init {
  activate static void Run() {
    Philosophers p;
    Forks f;
    int i;
  
    atomic {
      // Allocate the arrays of forks and philosophers
      p = new Philosophers;
      f = new Forks;

      // Allocate the individual fork and philosopher objects
      i = 0;
      while (i < sizeof(Philosophers)) {
	p[i] = new Philosopher;
	f[i] = new Fork;
	i = i + 1;
      }

      // Associate the philosophers with their forks and let them begin
      i = 0;
      while (i < sizeof(Philosophers)) {
	p[i].leftFork = f[i];
	p[i].rightFork = f[(i+1) % sizeof(Philosophers)];
    
	async p[i].Run();
	i = i + 1;
      }
    }
  }
};
\end{lstlisting}
%-----------------------------------
%	SECTION 2
%-----------------------------------

\subsection{PyCParser}
\pyc~is a light weight parser for the C language, developed purely in Python. It is like a 
small module which can be easily integrated into larger applications which require parsing C source code.
Its simple structure and the fact that it is written in pure Python makes it very easy to experiment with and
tweak.

\pyc~closely follows the C grammar provided at the end of the C99 standard document and aims to support the
full C99 language (as per ISO/IEC 9899). \pyc~, however, does not support any GCC extensions.

\subsubsection{AST Nodes}
\pyc~follows a set or rules for easily defining AST Nodes. Each construct has its own class which extends the AST Node class.
Each attribute defined with the node is another AST Node sub-class.
The nomenclature is:
\begin{verbatim}
<name>* - Child Node
<name>** - Sequence of Child Nodes
<name> - An attribute
\end{verbatim}

So, as an example, the AST Node for the {\tt FOR} construct would be:

$$For: [init*, cond*, next*, stmt*]$$

The corresponding class as written in PyCParser is:

\begin{lstlisting}
class For(Node):
    def __init__(self, init, cond, next, stmt, coord=None):
        self.init = init
        self.cond = cond
        self.next = next
        self.stmt = stmt
        self.coord = coord

    def children(self):
        nodelist = []
        if self.init is not None: nodelist.append(("init", self.init))
        if self.cond is not None: nodelist.append(("cond", self.cond))
        if self.next is not None: nodelist.append(("next", self.next))
        if self.stmt is not None: nodelist.append(("stmt", self.stmt))
        return tuple(nodelist)

    attr_names = ()
\end{lstlisting}
%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------

\section{\zmex}

\zmex~is an attempt to automatically extract models from a \cilkp~program. Distributed programming is a messy affair in C and syntactically, quite ugly. The \cilk~language was an attempt to 
make it cleaner and syntactically more appeasing. We support \cilk~ with a few added keywords and call it \cilkp~(described in detail in the next section). However, the ideas we present
are applicable to most programming languages.

\zmex~is written in {\tt Python} on top of \pyc~\citep{PyCParser}. The architecture is described in the next subsection.

%-----------------------------------
%	SUBSECTION 1
%-----------------------------------
\subsection{\zmex~architecture}

\zmex~takes in a user program written in \cilkp, passes it to \pyc which generates the Abstract Syntax Tree (or {\bf AST}). The AST object is passed to \zmex~which modifies
the AST and outputs a \zing~program.
\begin{figure}[htbp]
	\centering
		\includegraphics[width=6in,keepaspectratio]{Figures/zingerarch.png}
	\caption[\zmex~Architecture]{\zmex~Architecture}
	\label{fig:zingerarch}
\end{figure}
\section{\cilkp}
\cilkp~or {\bf cilk plus} is the programming language \zmex takes as input. \cilkp~is identical to \cilk, i.e., it is identical to C99 in most regards but has different
constructs for distributed programming are different. It must be noted that \cilkp~does not support pointers or multi-dimensional arrays. These aren't too tough to support and research has already been done \citep{zing2c}
to support both pointers and multi-dimensional arrays but that is not the focus of this thesis.

Below we give a table with new \cilkp~constructs and their \cilk~counterparts
\begin{center}
\begin{tabular}{l | l}
\cilkp~constructs & \cilk~constructs\\
\hline
atomic & - \\
\hline
spawn & spawn \\
\hline
sync & sync \\
\hline
spawn_for & cilk_for \footnotemark\\
\hline
yield & - \\
\hline
- & inlets\\
\end{tabular}
\end{center}

In the next three chapters we will describe each of the \cilkp constructs and how to translate them to the \zing~modeling language.

\footnotetext{Supported by Intel Cilk but not by MIT Cilk}