\newcommand{\etalchar}[1]{$^{#1}$}
\begin{thebibliography}{DCFLHRM10}

\bibitem[BHK98]{breese1998empirical}
John~S Breese, David Heckerman, and Carl Kadie.
\newblock Empirical analysis of predictive algorithms for collaborative
  filtering.
\newblock In {\em Proceedings of the Fourteenth conference on Uncertainty in
  artificial intelligence}, pages 43--52. Morgan Kaufmann Publishers Inc.,
  1998.

\bibitem[CHW01]{chee2001rectree}
Sonny Han~Seng Chee, Jiawei Han, and Ke~Wang.
\newblock Rectree: An efficient collaborative filtering method.
\newblock In {\em Data Warehousing and Knowledge Discovery}, pages 141--151.
  Springer, 2001.

\bibitem[DCFLHRM10]{de2010combining}
Luis~M De~Campos, Juan~M Fern{\'a}ndez-Luna, Juan~F Huete, and Miguel~A
  Rueda-Morales.
\newblock Combining content-based and collaborative recommendations: A hybrid
  approach based on bayesian networks.
\newblock {\em International Journal of Approximate Reasoning}, 51(7):785--799,
  2010.

\bibitem[DDGR07]{das2007google}
Abhinandan~S Das, Mayur Datar, Ashutosh Garg, and Shyam Rajaram.
\newblock Google news personalization: scalable online collaborative filtering.
\newblock In {\em Proceedings of the 16th international conference on World
  Wide Web}, pages 271--280. ACM, 2007.

\bibitem[HH05]{hofmann2005collaborative}
Thomas Hofmann and D~Hartmann.
\newblock Collaborative filtering with privacy via factor analysis.
\newblock In {\em Proceedings of the 2005 ACM Symposium on Applied Computing},
  pages 791--795, 2005.

\bibitem[JE09a]{jamali2009trustwalker}
Mohsen Jamali and Martin Ester.
\newblock Trustwalker: a random walk model for combining trust-based and
  item-based recommendation.
\newblock In {\em Proceedings of the 15th ACM SIGKDD international conference
  on Knowledge discovery and data mining}, pages 397--406. ACM, 2009.

\bibitem[JE09b]{jamali2009using}
Mohsen Jamali and Martin Ester.
\newblock Using a trust network to improve top-n recommendation.
\newblock In {\em Proceedings of the third ACM conference on Recommender
  systems}, pages 181--188. ACM, 2009.

\bibitem[JZFF10]{jannach2010recommender}
Dietmar Jannach, Markus Zanker, Alexander Felfernig, and Gerhard Friedrich.
\newblock {\em Recommender systems: an introduction}.
\newblock Cambridge University Press, 2010.

\bibitem[KBV09]{koren2009matrix}
Yehuda Koren, Robert Bell, and Chris Volinsky.
\newblock Matrix factorization techniques for recommender systems.
\newblock {\em Computer}, 42(8):30--37, 2009.

\bibitem[KSJ09]{konstas2009social}
Ioannis Konstas, Vassilios Stathopoulos, and Joemon~M Jose.
\newblock On social networks and collaborative recommendation.
\newblock In {\em Proceedings of the 32nd international ACM SIGIR conference on
  Research and development in information retrieval}, pages 195--202. ACM,
  2009.

\bibitem[KSS97]{kautz1997referral}
Henry Kautz, Bart Selman, and Mehul Shah.
\newblock Referral web: combining social networks and collaborative filtering.
\newblock {\em Communications of the ACM}, 40(3):63--65, 1997.

\bibitem[Lem05]{lemire2005scale}
Daniel Lemire.
\newblock Scale and translation invariant collaborative filtering systems.
\newblock {\em Information Retrieval}, 8(1):129--150, 2005.

\bibitem[LL10]{Liu}
Fengkun Liu and Hong~Joo Lee.
\newblock Use of social network information to enhance collaborative filtering
  performance.
\newblock {\em Expert Systems with Applications}, 37(7):4772 -- 4778, 2010.

\bibitem[LM05]{lemire2005slope}
Daniel Lemire and Anna Maclachlan.
\newblock Slope one predictors for online rating-based collaborative filtering.
\newblock In {\em SDM}, volume~5, pages 1--5. SIAM, 2005.

\bibitem[MKL09]{ma2009learning}
Hao Ma, Irwin King, and Michael~R Lyu.
\newblock Learning to recommend with social trust ensemble.
\newblock In {\em Proceedings of the 32nd international ACM SIGIR conference on
  Research and development in information retrieval}, pages 203--210. ACM,
  2009.

\bibitem[MYLK08]{ma2008sorec}
Hao Ma, Haixuan Yang, Michael~R Lyu, and Irwin King.
\newblock Sorec: social recommendation using probabilistic matrix
  factorization.
\newblock In {\em Proceedings of the 17th ACM conference on Information and
  knowledge management}, pages 931--940. ACM, 2008.

\bibitem[MZL{\etalchar{+}}11]{ma2011recommender}
Hao Ma, Dengyong Zhou, Chao Liu, Michael~R Lyu, and Irwin King.
\newblock Recommender systems with social regularization.
\newblock In {\em Proceedings of the fourth ACM international conference on Web
  search and data mining}, pages 287--296. ACM, 2011.

\bibitem[NST{\etalchar{+}}12]{noel2012new}
Joseph Noel, Scott Sanner, Khoi-Nguyen Tran, Peter Christen, Lexing Xie,
  Edwin~V Bonilla, Ehsan Abbasnejad, and Nicol{\'a}s Della~Penna.
\newblock New objective functions for social collaborative filtering.
\newblock In {\em Proceedings of the 21st international conference on World
  Wide Web}, pages 859--868. ACM, 2012.

\bibitem[Rob99]{robinson1999automated}
Gary~B Robinson.
\newblock Automated collaborative filtering in world wide web advertising,
  June~29 1999.
\newblock US Patent 5,918,014.

\bibitem[SK09]{su2009survey}
Xiaoyuan Su and Taghi~M Khoshgoftaar.
\newblock A survey of collaborative filtering techniques.
\newblock {\em Advances in artificial intelligence}, 2009:4, 2009.

\bibitem[SKKR01]{sarwar2001item}
Badrul Sarwar, George Karypis, Joseph Konstan, and John Riedl.
\newblock Item-based collaborative filtering recommendation algorithms.
\newblock In {\em Proceedings of the 10th international conference on World
  Wide Web}, pages 285--295. ACM, 2001.

\bibitem[UF98]{ungar1998clustering}
Lyle~H Ungar and Dean~P Foster.
\newblock Clustering methods for collaborative filtering.
\newblock In {\em AAAI Workshop on Recommendation Systems}, volume~1, 1998.

\bibitem[YCZ11]{yuan2011factorization}
Quan Yuan, Li~Chen, and Shiwan Zhao.
\newblock Factorization vs. regularization: fusing heterogeneous social
  relationships in top-n recommendation.
\newblock In {\em Proceedings of the fifth ACM conference on Recommender
  systems}, pages 245--252. ACM, 2011.

\bibitem[YSGL12]{yang2012top}
Xiwang Yang, Harald Steck, Yang Guo, and Yong Liu.
\newblock On top-k recommendation using social networks.
\newblock In {\em Proceedings of the sixth ACM conference on Recommender
  systems}, pages 67--74. ACM, 2012.

\bibitem[ZWSP08]{netflix}
Yunhong Zhou, Dennis Wilkinson, Robert Schreiber, and Rong Pan.
\newblock Large-scale parallel collaborative filtering for the netflix prize.
\newblock In {\em Algorithmic Aspects in Information and Management}, pages
  337--348. Springer, 2008.

\end{thebibliography}
