\contentsline {chapter}{Abstract}{vii}{dummy.2}
\vspace {0em}
\contentsline {chapter}{Acknowledgements}{ix}{dummy.3}
\vspace {0em}
\contentsline {chapter}{Contents}{xi}{dummy.4}
\contentsline {chapter}{List of Figures}{xiii}{dummy.6}
\contentsline {chapter}{List of Algorithms}{xv}{dummy.8}
\vspace {2em}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.10}
\contentsline {section}{\numberline {1.1}Motivation}{2}{section.11}
\contentsline {section}{\numberline {1.2}Problem Statement}{2}{section.12}
\contentsline {section}{\numberline {1.3}Contributions of this Thesis}{3}{section.13}
\contentsline {section}{\numberline {1.4}Outline of the Thesis}{3}{section.14}
\contentsline {chapter}{\numberline {2}Background}{5}{chapter.15}
\contentsline {section}{\numberline {2.1}Challenges in Collaborative Filtering}{5}{section.16}
\contentsline {subsection}{\numberline {2.1.1}Data Sparsity and Cold Start}{6}{subsection.17}
\contentsline {subsection}{\numberline {2.1.2}Scalability}{6}{subsection.18}
\contentsline {section}{\numberline {2.2}Overview of the techniques}{6}{section.19}
\contentsline {subsection}{\numberline {2.2.1}Memory-Based Collaborative Filtering Techniques}{7}{subsection.20}
\contentsline {subsubsection}{\numberline {2.2.1.1}Default Voting}{8}{subsubsection.22}
\contentsline {subsubsection}{\numberline {2.2.1.2}Inverse User Frequency}{8}{subsubsection.23}
\contentsline {subsubsection}{\numberline {2.2.1.3}Case Amplification}{8}{subsubsection.24}
\contentsline {subsection}{\numberline {2.2.2}Model-Based Collaborative Filtering Techniques}{9}{subsection.25}
\contentsline {subsubsection}{\numberline {2.2.2.1}Clustering Techniques}{9}{subsubsection.26}
\contentsline {subsubsection}{\numberline {2.2.2.2}Regression-Based Techniques}{10}{subsubsection.27}
\contentsline {subsubsection}{\numberline {2.2.2.3}Latent Factor Models }{10}{subsubsection.28}
\contentsline {subsection}{\numberline {2.2.3}Social Network-Based Collaborative Filtering}{11}{subsection.29}
\contentsline {subsubsection}{\numberline {2.2.3.1}Neighborhood-Based Social Recommender Systems}{11}{subsubsection.30}
\contentsline {subsubsection}{\numberline {2.2.3.2}Matrix Factorization-Based Social Recommender Systems}{12}{subsubsection.31}

\contentsline {chapter}{\numberline {3}Low Rank Matrix Factorization}{13}{chapter.32}
\contentsline {section}{\numberline {3.1}Problem Formulation}{13}{section.33}
\contentsline {section}{\numberline {3.2}Learning Algorithms}{14}{section.38}
\contentsline {subsection}{\numberline {3.2.1}Alternating Least Squares}{15}{subsection.39}
\contentsline {subsection}{\numberline {3.2.2}Stochastic Gradient Descent}{16}{subsection.41}
\contentsline {subsection}{\numberline {3.2.3}Alternating Least Squares versus Stochastic Gradient Descent}{16}{subsection.45}
\contentsline {section}{\numberline {3.3}Update Rules for ALS}{17}{section.46}
\contentsline {section}{\numberline {3.4}Scalability}{18}{section.54}
\contentsline {section}{\numberline {3.5}Different Loss Functions}{19}{section.57}
\contentsline {chapter}{\numberline {4}Our Approach}{21}{chapter.58}
\contentsline {section}{\numberline {4.1}Problem Formulation}{21}{section.59}
\contentsline {section}{\numberline {4.2}Social Matrix Factorization}{22}{section.60}
\contentsline {subsection}{\numberline {4.2.1}Social Connection Term}{22}{subsection.62}
\contentsline {subsection}{\numberline {4.2.2}Justification for the Social Connection Term }{23}{subsection.64}
\contentsline {section}{\numberline {4.3}Learning Algorithm}{23}{section.65}
\contentsline {section}{\numberline {4.4}Update Rules for Social ALS}{24}{section.67}
\contentsline {subsection}{\numberline {4.4.1}Update Rule for User Feature Matrix}{25}{subsection.69}
\contentsline {subsection}{\numberline {4.4.2}Update Rule for Item Feature Matrix}{26}{subsection.77}
\contentsline {section}{\numberline {4.5}Parameter Tuning}{27}{section.84}
\contentsline {chapter}{\numberline {5}Experiments and Results}{29}{chapter.89}
\contentsline {section}{\numberline {5.1}Setup}{29}{section.90}
\contentsline {subsection}{\numberline {5.1.1}Datasets}{30}{subsection.91}
\contentsline {subsection}{\numberline {5.1.2}Parameters}{31}{subsection.96}
\contentsline {section}{\numberline {5.2}Parameter Tuning}{31}{section.97}
\contentsline {subsection}{\numberline {5.2.1}Lambda}{32}{subsection.98}
\contentsline {subsection}{\numberline {5.2.2}Number of Features}{33}{subsection.101}
\contentsline {subsection}{\numberline {5.2.3}Alpha}{35}{subsection.104}
\contentsline {subsubsection}{\numberline {5.2.3.1}Douban Dataset}{35}{subsubsection.105}
\contentsline {subsubsection}{\numberline {5.2.3.2}Epinions Dataset}{37}{subsubsection.109}
\contentsline {section}{\numberline {5.3}Effect of the Social Connection Term}{39}{section.113}
\contentsline {chapter}{\numberline {6}Conclusion and Future Work}{41}{chapter.114}
