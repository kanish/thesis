%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Masters/Doctoral Thesis 
% LaTeX Template
% Version 1.42 (19/1/14)
%
% This template has been downloaded from:
% http://www.latextemplates.com
%
% Original authors:
% Steven Gunn 
% http://users.ecs.soton.ac.uk/srg/softwaretools/document/templates/
% and
% Sunil Patel
% http://www.sunilpatel.co.uk/thesis-template/
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
% Note:
% Make sure to edit document variables in the Thesis.cls file
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[12pt, a4paper, twoside]{Thesis} % Paper size, default font size and one-sided paper

\graphicspath{{Pictures/}} % Specifies the directory where pictures are stored
\usepackage[square, numbers, comma, sort&compress]{natbib} % Use the natbib reference package - read up on this to edit the reference style; if you want text (e.g. Smith et al., 2012) for the in-text references (instead of numbers), remove 'numbers' 
\usepackage{syntax}
\usepackage{setspace} % for doublespacing singlespacing etc.
\renewcommand{\lstlistingname}{Program}% Listing -> Algorithm
\renewcommand{\lstlistlistingname}{List of \lstlistingname s}% List of Listings -> List of Programs
\hypersetup{urlcolor=black, colorlinks=true} % Colors hyperlinks in blue - change to black if annoying
\title{\ttitle} % Defines the thesis title - don't touch this

\begin{document}

\frontmatter % Use roman page numbering style (i, ii, iii, iv...) for the pre-content pages

\setstretch{1.3} % Line spacing of 1.3

% Define the page headers using the FancyHdr package and set up for one-sided printing
\fancyhead{} % Clears all page headers and footers
\rhead{\thepage} % Sets the right side header to show the page number
\lhead{} % Clears the left side page header

\pagestyle{fancy} % Finally, use the "fancy" page style to implement the FancyHdr headers

\newcommand{\HRule}{\rule{\linewidth}{0.5mm}} % New command to make the lines in the title page

% PDF meta-data
\hypersetup{pdftitle={\ttitle}}
\hypersetup{pdfsubject=\subjectname}
\hypersetup{pdfauthor=\authornames}
\hypersetup{pdfkeywords=\keywordnames}

%----------------------------------------------------------------------------------------
%	TITLE PAGE
%----------------------------------------------------------------------------------------

\begin{titlepage}
\begin{center}

\textsc{\LARGE \univname}\\[1.5cm] % University name
\textsc{\Large Masters Thesis}\\[0.5cm] % Thesis type

\HRule \\[0.4cm] % Horizontal line
{\huge \bfseries \ttitle}\\[0.4cm] % Thesis title
\HRule \\[1.5cm] % Horizontal line
 
\begin{minipage}{0.4\textwidth}
\begin{flushleft} \large
\emph{Author:}\\
{\authornames} % Author name - remove the \href bracket to remove the link
\end{flushleft}
\end{minipage}
\begin{minipage}{0.4\textwidth}
\begin{flushright} \large
\emph{Supervisor:} \\
{\secsupname} \\ % Supervisor name - remove the \href bracket to remove the link  \\
{\it and} \\
{\primesupname}
\end{flushright}
\end{minipage}\\[1cm]
 \begin{figure}[htbp]
	\centering
		\includegraphics[width=1.6in,keepaspectratio]{Figures/redlogo.jpg}
\end{figure} 
\large \textit{A thesis submitted in fulfillment of the requirements\\ for the degree of \degreename}\\[0.3cm] % University requirement text
\textit{in the}\\[0.4cm]
\deptname\\[1cm] % Research group name and department name
 
{\large \today}\\[2cm] % Date
\vfill
\end{center}

\end{titlepage}
\cleardoublepage


%----------------------------------------------------------------------------------------
%	CERTIFICATE
%	Your institution may give you a different text to place here
%----------------------------------------------------------------------------------------
\Certificate{
\pagestyle{empty}
\addtocontents{toc}{\vspace{1em}} % Add a gap in the Contents, for aesthetics
It is certified that the work contained in this thesis entitled ``\ttitle'', by \authornames, has been carried out under our supervision and that this work has not been submitted elsewhere for a degree.
\vskip 1in
\begin{flushleft}
		\hspace*{5.3cm}{\hrulefill}\\
		\hspace*{5.3cm}\secsupname\\
		\hspace*{5.3cm}Department of Computer Science and Engineering,\\ 
		\hspace*{5.3cm}Indian Institute of Technology Kanpur\\
		\hspace*{5.3cm}Kanpur-208016
\vskip 1in
		\hspace*{5.3cm}{\hrulefill}\\
		\hspace*{5.3cm}\primesupname\\
		\hspace*{5.3cm}Department of Elect Engineering,\\ 
		\hspace*{5.3cm}Indian Institute of Technology Kanpur\\
		\hspace*{5.3cm}Kanpur-208016
\end{flushleft}
\begin{center}
{\large \today}
\end{center}
}
\cleardoublepage

%----------------------------------------------------------------------------------------
%	QUOTATION PAGE
%----------------------------------------------------------------------------------------

\pagestyle{empty} % No headers or footers for the following pages

\null\vfill\vfill % Add some space to move the quote down the page a bit

\textit{``Come on, man, I'm not trying to scam anybody here"}

\begin{flushright}
-- Dude. {\it The Big Lebowski}
\end{flushright}

\vfill\vfill\vfill\vfill\vfill\vfill\null % Add some space at the bottom to position the quote just right
\cleardoublepage


%----------------------------------------------------------------------------------------
%	ABSTRACT PAGE
%----------------------------------------------------------------------------------------

\addtotoc{Abstract} % Add the "Abstract" page entry to the Contents

\abstract{\addtocontents{toc}{\vspace{1em}} % Add a gap in the Contents, for aesthetics
Model Checking is an indispensable tool geared towards analysis of programs. With increasing complexity of computer software, it has become
a necessity to automate the process of extracting models. This thesis attempts to solve this problem by developing a tool which can automatically
extract models of concurrent programs written in the \cilk~language. It also sets up a feedback loop to isolate the execution path of the buggy run.
We present efficient algorithms to translate constructs for concurrent programming to their equivalent models in the \zing~model checking language.
}
\cleardoublepage

%----------------------------------------------------------------------------------------
%	ACKNOWLEDGEMENTS
%----------------------------------------------------------------------------------------

\setstretch{1.3} % Reset the line-spacing to 1.3 for body text (if it has changed)

\acknowledgements{\addtocontents{toc}{\vspace{1em}} % Add a gap in the Contents, for aesthetics
I am indebted to my thesis supervisors {\bf Dr. Subhajit Roy} and {\bf Dr. S. S. K. Iyer} for their valuable
guidance and support towards the completion of the thesis work.

I sincerely thank {\bf Dr. Subhajit Roy}. Without his guidance and motivation, this thesis would never have seen daylight.
I thank him very much for his support and understanding over the last few years. I am honored to have worked under his 
excellent guidance.

I would like to thank {\bf Dr. S. S. K. Iyer} for his constant support and motivating me to pursue something out of 
the norm.

I would like to thank all my friends, specially, Neelu, Freddy, Baba, Muski, Umar, Digraj, Prarthana, Haji, Paggy, Pal and Mankeet for giving me 
so many wonderful memories to cherish all my life. I would also like to thank John, Paul, Ringo and George for always being with me, during highs and during lows.

I would like to thank IIT Kanpur for providing me with such an amazing environment to spend the first twenty odd years of my life. It will 
always be my first home, my first school, my first college.

Lastly, I would like to thank my parents for being with me at every step of my life looking over my shoulders. Their unparalleled support and 
motivation has been the biggest driving force in my life.

\vskip 2cm
\begin{flushright}
Pratik Moona\\
\today
\end{flushright}

}
\cleardoublepage
%----------------------------------------------------------------------------------------
%	LIST OF CONTENTS/FIGURES/TABLES PAGES
%----------------------------------------------------------------------------------------

\pagestyle{fancy} % The page style headers have been "empty" all this time, now use the "fancy" headers as defined before to bring them back

\lhead{\emph{Contents}} % Set the left side page header to "Contents"
\tableofcontents % Write out the Table of Contents

\lhead{\emph{List of Figures}} % Set the left side page header to "List of Figures"
\listoffigures % Write out the List of Figures

\lhead{\emph{List of Algorithms}} % Set the left side page header to "List of Tables"
\btypeout{List of Algorithms}
\addtotoc{List of Algorithms}
\listofalgorithms % Write out the List of Tables
\cleardoublepage

\lhead{\emph{List of Programs}} % Set the left side page header to "List of Tables"
\lstlistoflistings % Write out the List of Tables

\clearpage

%----------------------------------------------------------------------------------------
%	DEDICATION
%----------------------------------------------------------------------------------------

\setstretch{1.3} % Return the line spacing back to 1.3

\pagestyle{empty} % Page style needs to be empty for this page

\dedicatory{To my parents} % Dedication text

\addtocontents{toc}{\vspace{2em}} % Add a gap in the Contents, for aesthetics

%----------------------------------------------------------------------------------------
%	THESIS CONTENT - CHAPTERS
%----------------------------------------------------------------------------------------

\mainmatter % Begin numeric (1,2,3...) page numbering

\pagestyle{fancy} % Return the page headers back to the "fancy" style

% Include the chapters of the thesis as separate files from the Chapters folder
% Uncomment the lines as you write the chapters
\doublespacing
\input{Chapters/Chapter1}
\input{Chapters/Chapter2} 
\input{Chapters/Chapter3}
\input{Chapters/Chapter4} 
\input{Chapters/Chapter5}
\input{Chapters/Chapter6}
\input{Chapters/Chapter7}
\input{Chapters/Chapter8}
%----------------------------------------------------------------------------------------
%	THESIS CONTENT - APPENDICES
%----------------------------------------------------------------------------------------

\addtocontents{toc}{\vspace{2em}} % Add a gap in the Contents, for aesthetics

\appendix % Cue to tell LaTeX that the following 'chapters' are Appendices

% Include the appendices of the thesis as separate files from the Appendices folder
% Uncomment the lines as you write the Appendices

%\input{Appendices/AppendixA}
%\input{Appendices/AppendixB}
%\input{Appendices/AppendixC}

\addtocontents{toc}{\vspace{2em}} % Add a gap in the Contents, for aesthetics

\backmatter

%----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY
%----------------------------------------------------------------------------------------

\label{Bibliography}

\lhead{\emph{Bibliography}} % Change the page header to say "Bibliography"

\bibliographystyle{unsrtnat} % Use the "unsrtnat" BibTeX style for formatting the Bibliography

\bibliography{Bibliography} % The references (bibliography) information are stored in the file named "Bibliography.bib"

\end{document}  